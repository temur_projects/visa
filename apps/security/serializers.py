from rest_framework import serializers
from apps.staff.models import StaffInfo

class StaffInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = StaffInfo
        fields = '__all__'