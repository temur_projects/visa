from django.shortcuts import render, HttpResponse, reverse, HttpResponseRedirect
from django.views import View
from django.http import JsonResponse
import pytesseract
from PIL import Image, ImageEnhance
from apps.staff.models import StaffCategory, StaffInfo
from django.conf import settings
import cv2
import numpy as np
from apps.guest.forms import GuestForm
from .countries import countries
from .models import Image as Im
from apps.guest.models import GuestInfo
from PIL import Image, ImageEnhance
from rest_framework.views import APIView
import scipy.misc
from .serializers import StaffInfoSerializer
import json
import urllib

def one(request):
    img = Image.objects.get(id=1)
    return HttpResponse(img.image, content_type='image/jpeg')


def two(request):
    img = Image.objects.get(id=2)
    return HttpResponse(img.image, content_type='image/jpeg')


def three(request):
    img = Image.objects.get(id=3)
    return HttpResponse(img.image, content_type='image/jpeg')


def get_names(name):
    names = name.split("<<")
    return {
        'last_name': names[0],
        'first_name': names[1]
    }


def get_sex(sex):
    if sex == "M" or sex == "H":
        return "Male"
    elif sex == "F":
        return "Female"
    elif sex == "X":
        return "Unspecified"
    else:
        return sex


def get_passport_number(string):
    if '<' in string:
        string = string.replace('<', '')

    string = string[:2] + string[2:].replace("O", "0")
    return string


def get_country(country):
    if country in countries:
        return countries[country]


class ScanPassportView(View):
    template_name = 'scan_passport.html'

    def get(self, request):
        response = render(request, self.template_name)
        return response

    def post(self, request):
        try:
            if "first_name" in self.request.POST.keys():
                print(self.request.POST)
                image=self.request.POST.get('image')
                print(self.request.POST.get('image')[7:])
                img=Im.objects.get(image=image[7:])
                print(img)
                form = GuestForm(self.request.POST)
                guest = form.save(commit=False)
                guest.image=img.image

                guest.save()
                print(guest.id)
                return HttpResponseRedirect(reverse('choose-staff', args=[guest.id, ]))

            img = request.FILES.get('image')
            img = Im.objects.create(image=img)
            # img.save()
            print(img.image.url)
            grey = cv2.imread(settings.BASE_DIR + img.image.url, 0)
            scipy.misc.imsave('/home/user/1.png', grey)
            image = Image.open('/home/user/1.png')
            contrast = ImageEnhance.Contrast(image)
            b = contrast.enhance(3.5)
            b = np.array(b)
            grey_new = np.where((255 - b) < 240, 255, b + 240)
            scipy.misc.imsave('/home/user/1.png', grey)
            mrz1 = pytesseract.image_to_string(grey_new).replace(' ', '')
            index1 = mrz1.find('P<')
            mrz1 = mrz1[index1:].split('\n')
            while '' in mrz1:
                mrz1.remove("")
            birth_date = mrz1[1][13:19]
            birth_date = '%s/%s/%s' % (birth_date[4:6], birth_date[2:4], birth_date[:2])
            expiration = mrz1[1][21:27]
            expiration = '%s/%s/%s' % (expiration[4:6], expiration[2:4], expiration[:2])
            data = {
                'result': 'ok',
                'firstName': get_names(mrz1[0][5:44])['first_name'],
                'lastName': get_names(mrz1[0][5:44])['last_name'],
                # 'nation': mrz[-1][12:15],
                'nation': get_country(mrz1[0][2:5]),
                'birthDate': birth_date,
                'passportNumber': get_passport_number(mrz1[1][:9]),
                'sex': get_sex(mrz1[1][20]),
                'expiration': expiration,
                'img': img.image.url,

            }
            return JsonResponse(data)
        except Exception as e:
            print(e)
            return JsonResponse({'result': 'fail'})


class ChooseStaffView(View):
    template_name = 'choose_staff.html'

    def get(self, request, id):
        print(request.GET)
        if request.GET:
            member = StaffInfo.objects.get(id=request.GET.get("id"))
            print(member)
            member = StaffInfoSerializer(member)
            print(member.data)

            return JsonResponse(member.data)
        response = render(request, self.template_name, {'guest': GuestInfo.objects.get(id=id),
                                                        'categories': StaffCategory.objects.all()})
        return response
    
    def post(self, request):
       print(request.POST)
       return JsonResponse({'result': 'fail'})