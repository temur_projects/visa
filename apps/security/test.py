

# global thresholding
# ret1,th1 = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
#
# # Otsu's thresholding
# ret2,th2 = cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
#
# # Otsu's thresholding after Gaussian filtering
# blur = cv2.GaussianBlur(img,(5,5),0)
# ret3,th3 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
#
# # plot all the images and their histograms
# images = [img, 0, th1,
#           img, 0, th2,
#           blur, 0, th3]
# print(pytesseract.image_to_string(th1))
# cv2.imshow("L",th2)
# cv2.waitKey(0)
#
# titles = ['Original Noisy Image','Histogram','Global Thresholding (v=127)',
#           'Original Noisy Image','Histogram',"Otsu's Thresholding",
#           'Gaussian filtered Image','Histogram',"Otsu's Thresholding"]
#
# for i in range(3):
#     plt.subplot(3,3,i*3+1),plt.imshow(images[i*3],'gray')
#     plt.title(titles[i*3]), plt.xticks([]), plt.yticks([])
#     plt.subplot(3,3,i*3+2),plt.hist(images[i*3].ravel(),256)
#     plt.title(titles[i*3+1]), plt.xticks([]), plt.yticks([])
#     plt.subplot(3,3,i*3+3),plt.imshow(images[i*3+2],'gray')
#     plt.title(titles[i*3+2]), plt.xticks([]), plt.yticks([])
# plt.show()


#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# import cv2
# import pytesseract
# from PIL import Image
#
#
# def main():
#     # Get File Name from Command Line
#     path = input("Enter the file path : ").strip()
#     # load the image
#     image = cv2.imread(path)
#     # Convert image to grayscale
#     gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#     temp = input(
#         "Do you want to pre-process the image ?\nThreshold : 1\nGrey : 2\nNone : 0\nEnter your choice : ").strip()
#     # If user enter 1, Process Threshold
#     if temp == "1":
#         gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
#     elif temp == "2":
#         gray = cv2.medianBlur(gray, 1)
#     # store grayscale image as a temp file to apply OCR
#     filename = "{}.png".format("temp")
#     cv2.imwrite(filename, gray)
#     # load the image as a PIL/Pillow image, apply OCR, and then delete the temporary file
#     text = pytesseract.image_to_string(Image.open(filename))
#     print(text)
#
#
# try:
#     main()
# except Exception as e:
#     print(e.args)
#     print(e.__cause__)





from imutils import paths
import numpy as np
import argparse
import imutils
import cv2
import pytesseract

# construct the argument parse and parse the arguments
# ap = argparse.ArgumentParser()
# args = vars(ap.parse_args())
#
# # initialize a rectangular and square structuring kernel
# rectKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (13, 5))
# sqKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (21, 21))
# image = cv2.imread("/home/user/projects/visa/visa/media/images/1.jpg")
# image = imutils.resize(image, height=600)
# gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#
# # smooth the image using a 3x3 Gaussian, then apply the blackhat
# # morphological operator to find dark regions on a light background
# gray = cv2.GaussianBlur(gray, (3, 3), 0)
# blackhat = cv2.morphologyEx(gray, cv2.MORPH_BLACKHAT, rectKernel)
#
# # compute the Scharr gradient of the blackhat image and scale the
# # result into the range [0, 255]
# gradX = cv2.Sobel(blackhat, ddepth=cv2.CV_32F, dx=1, dy=0, ksize=-1)
# gradX = np.absolute(gradX)
# (minVal, maxVal) = (np.min(gradX), np.max(gradX))
# gradX = (255 * ((gradX - minVal) / (maxVal - minVal))).astype("uint8")
#
# # apply a closing operation using the rectangular kernel to close
# # gaps in between letters -- then apply Otsu's thresholding method
# gradX = cv2.morphologyEx(gradX, cv2.MORPH_CLOSE, rectKernel)
# thresh = cv2.threshold(gradX, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
#
# # perform another closing operation, this time using the square
# # kernel to close gaps between lines of the MRZ, then perform a
# # serieso of erosions to break apart connected components
# thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, sqKernel)
# thresh = cv2.erode(thresh, None, iterations=4)
#
# # during thresholding, it's possible that border pixels were
# # included in the thresholding, so let's set 5% of the left and
# # right borders to zero
# p = int(image.shape[1] * 0.05)
# thresh[:, 0:p] = 0
# thresh[:, image.shape[1] - p:] = 0
#
# # find contours in the thresholded image and sort them by their
# # size
# cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
#     cv2.CHAIN_APPROX_SIMPLE)[-2]
# cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
#
# # loop over the contours
# for c in cnts:
#     # compute the bounding box of the contour and use the contour to
#     # compute the aspect ratio and coverage ratio of the bounding box
#     # width to the width of the image
#     (x, y, w, h) = cv2.boundingRect(c)
#     ar = w / float(h)
#     crWidth = w / float(gray.shape[1])
#
#     # check to see if the aspect ratio and coverage width are within
#     # acceptable criteria
#     if ar > 5 and crWidth > 0.75:
#         # pad the bounding box since we applied erosions and now need
#         # to re-grow it
#         pX = int((x + w) * 0.03)
#         pY = int((y + h) * 0.03)
#         (x, y) = (x - pX, y - pY)
#         (w, h) = (w + (pX * 2), h + (pY * 2))
#
#         # extract the ROI from the image and draw a bounding box
#         # surrounding the MRZ
#         roi = image[y:y + h, x:x + w].copy()
#         cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
#         break
#
# e = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
# e = cv2.medianBlur(e,1)
# print(pytesseract.image_to_string(e))
# # show the output e
# cv2.imshow("Image", image)
# cv2.imshow("ROI", roi)
# cv2.waitKey(0)

from PIL import Image, ImageEnhance


# def change_contrast(img, level):
#     factor = (259 * (level + 255)) / (255 * (259 - level))
#     print(img)
#     def contrast(c):
#         print(c)
#         value = 128 + factor * (c - 128)
#         return max(0, min(255, value))
#     return img.point(contrast)
# a=Image.open("/home/user/3.jpg")
# change_contrast(a, 2)
# a=np.array(a)



grey = cv2.imread("/home/user/US-P-1.jpg", 0)

import scipy.misc
scipy.misc.imsave('/home/user/1.png', grey)
from PIL import Image, ImageEnhance
image = Image.open('/home/user/1.png')
contrast = ImageEnhance.Contrast(image)
image.show()
b=contrast.enhance(3.5)
b.show()
b=np.array(b)
new = grey
print(pytesseract.image_to_string(b))


new = grey
grey_new = np.where((255 - b) <240,255,b+240)
scipy.misc.imsave('/home/user/1.png', grey_new)
image = Image.open('/home/user/1.png')
contrast = ImageEnhance.Contrast(image)
image.show()
b=contrast.enhance(3.5)
b.show()

b=np.array(b)

grey_new = np.where((255 - b) <100,255,b+100)
res = np.hstack((grey, new))

print(pytesseract.image_to_string(b))
cv2.imshow('image', b)
cv2.waitKey(0)
cv2.destroyAllWindows()
