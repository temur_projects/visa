from django.urls import path
from .views import ScanPassportView, ChooseStaffView


urlpatterns = [
    path('', ScanPassportView.as_view(), name="scan-passport"),
    path('<int:id>/', ChooseStaffView.as_view(), name="choose-staff")
    # path('staff/', include()),
    # path('security/', include()),
    # path('guest/', include())
]
