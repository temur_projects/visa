def post(self, request):
    try:
        img = request.FILES.get('image')
        img = Image.objects.create(image=img)
        img.save()
        import os
        file = img.image.path.split('/')[-1]
        filename = file.split('.')[0]
        print(2222, img.image.name)
        path = settings.BASE_DIR + '/media/images/'
        command = """ 
            cd {path};pwd;
            convert {file} -type GrayScale out.jpg;
            convert out.jpg -resize 1191 {filename}_resized.jpg;

    """.format(
            path=path,
            file=file,
            filename=filename,
            filename2=filename,
        )
        os.system(command.replace("\n", ""))
        file_path = path + '{}_resized.jpg'.format(filename)
        print(file_path, command, file_path)
        alpha = 1.5
        beta = 15
        print('PATH: ', path)
        os.system('echo "%s"' % path)
        im = cv2.imread(file_path)
        s = cv2.addWeighted(im, alpha, np.zeros(im.shape, im.dtype), 0, beta)
        dst = cv2.fastNlMeansDenoisingColored(s, None, 10, 10, 7, 21)
        cv2.imwrite("{}{}_resized_.jpg".format(path, filename), dst)
        # txt = image_to_string(Im.open(path + 'resized_.jpg'))
        print(path, filename)
        result = os.system('tesseract {}{}_resized_.jpg {}ooo'.format(path, filename, path))
        if result == 1:
            raise TypeError('Couln\'nt parse mrz!')
        f = open(path + 'ooo.txt')
        mrz = f.read().replace('\n', '')
        index = mrz.find('P<')
        mrz = ''.join(mrz[index:].replace('\n', ''))
        print(mrz)
        birth_date = mrz[57:63]
        birth_date = '%s/%s/%s' % (birth_date[4:6], birth_date[2:4], birth_date[:2])

        expiration = mrz[65:71]
        expiration = '%s/%s/%s' % (expiration[4:6], expiration[2:4], expiration[:2])
        a = open('a.txt', 'w')
        a.write(mrz)
        data = {
            'result': 'ok',
            'firstName': get_names(mrz[5:44])['first_name'],
            'lastName': get_names(mrz[5:44])['last_name'],
            # 'nation': mrz[-1][12:15],
            'nation': get_country(mrz[2:5]),
            'birthDate': birth_date,
            'passportNumber': get_passport_number(mrz[44:53]),
            'sex': get_sex(mrz[64]),
            'expiration': expiration,
        }
        # sex = ['Male', 'Female']
        # valid = False
        # valid = data['sex'] in sex
        # if valid:
        return JsonResponse(data)
        # return JsonResponse({'result': 'fail'})
    except:
        return JsonResponse({'result': 'fail'})