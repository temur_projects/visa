from django import forms
from .models import GuestInfo

class GuestForm(forms.ModelForm):
    class Meta:
        model = GuestInfo
        fields = '__all__'