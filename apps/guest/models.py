from django.db import models


class GuestInfo(models.Model):
    first_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100, blank=True)
    citizenship = models.CharField(max_length=100, blank=True)
    date_of_birth = models.CharField(max_length=100, blank=True)
    sex = models.CharField(max_length=100, blank=True)
    passport_number = models.CharField(max_length=100, blank=True)
    expiration_date = models.CharField(max_length=100, blank=True)
    image = models.ImageField(upload_to="guests/", blank=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"
