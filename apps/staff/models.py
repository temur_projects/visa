from django.db import models
from django.contrib.auth.models import User


class StaffCategory(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class StaffInfo(models.Model):
    #user = models.OneToOneField('auth.user', relsted_name="profile")
    category = models.ForeignKey(StaffCategory, related_name="members", on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100)
    middle_name = models.CharField(max_length=100, default="")
    last_name = models.CharField(max_length=100)
    room = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.first_name} {self.middle_name} {self.last_name}"
