from django.shortcuts import render
from django.views.generic import DetailView


def staff_room(request, id):
    return render(request, "staff_room.html")