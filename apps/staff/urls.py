from django.urls import path, include
from .views import staff_room


urlpatterns = [
    path('<int:id>/', staff_room, name="staff-room"),
]