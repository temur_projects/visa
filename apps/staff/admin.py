from django.contrib import admin
from .models import StaffCategory, StaffInfo
# Register your models here.
class s(admin.TabularInline):
    model = StaffInfo


class Staff(admin.ModelAdmin):
    inlines = [s]

admin.site.register(StaffCategory, Staff)