from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
    url(r'^staff/(?P<room_name>[^/]+)/$', consumers.ChatConsumer),
    url(r'^security/$', consumers.ChatConsumer1),

]